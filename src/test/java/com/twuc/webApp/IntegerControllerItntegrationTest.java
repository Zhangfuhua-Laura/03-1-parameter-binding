package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.constraints.Email;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegerControllerItntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_succeed_when_bind_integer_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users1/1"))
                .andExpect(MockMvcResultMatchers.content().string("Both int and Integer will be ok."));
    }

    @Test
    void should_succeed_when_bind_int_to_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users2/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Both int and Integer will be ok."));
    }

    @Test
    void should_succeed_when_bind_two_parameters() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/1/books/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("userId is 1 and bookId is 1"));
    }

    @Test
    void should_succeed_when_query_param_with_default_values() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user/queryParam/defaultValue"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("userId is 1"));
    }

    @Test
    void should_succeed_when_query_param_with_collection() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user/queryParam/collection?userId=1,2,3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("userId are [1, 2, 3]"));
    }

    @Test
    void should_return_json_when() throws JsonProcessingException {
        Contract contract = new Contract(1);
        String json = new ObjectMapper().writeValueAsString(contract);
        assertEquals("{\"id\":1}", json);
    }

    @Test
    void should_return_time_when_given_correct_date_time() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2019-10-01T10:00Z[UTC]"));
    }

    @Test
    void should_return_time_when_given_not_correct_time() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"dateTime\":\"2019-10-01\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_normally_when_given_not_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/validation/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new ValidationUser(1, "abc", 1, 2, ""))))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userId").value(1));
    }

    @Test
    void should_return_0_when_given_null() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/validation/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new ValidationUser(null, "abc", 1, 2, ""))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_given_long_description() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/validation/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new ValidationUser(1, "laaaang", 1, 2, ""))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_given_wrong_min() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/validation/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new ValidationUser(1, "ab", -1, 2, ""))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_given_uncorrected_email() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/validation/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new ValidationUser(1, "ad", 1, 2, "notEmail"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}
