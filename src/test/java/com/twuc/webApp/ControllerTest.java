package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ControllerTest {
    @Test
    void should_return_json_when() throws JsonProcessingException {
        Contract contract = new Contract(1);
        String json = new ObjectMapper().writeValueAsString(contract);
        assertEquals("{\"id\":1}", json);
    }
}
