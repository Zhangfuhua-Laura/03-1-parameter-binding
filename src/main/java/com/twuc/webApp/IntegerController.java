package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class IntegerController {
    @GetMapping("/users1/{userId}")
    public String getUserId(@PathVariable Integer userId) {
        return "Both int and Integer will be ok.";
    }

    @GetMapping("/users2/{userId}")
    public String getUserId(@PathVariable int userId) {
        return "Both int and Integer will be ok.";
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getQuery(@PathVariable int bookId, @PathVariable int userId) {
        return String.format("userId is %d and bookId is %d", userId, bookId);
    }

    @GetMapping("/user/queryParam/defaultValue")
    public String getUserIdByDefaultQueryParam(@RequestParam(defaultValue = "1") int userId) {
        return String.format("userId is %d", userId);
    }

    @GetMapping("/user/queryParam/collection")
    public String getUserIdByCollectionQueryParam(@RequestParam List<String> userId) {
        return String.format("userId are %s", userId.toString());
    }

    @PostMapping("/datetimes")
    public String getDateTimes(@RequestBody MyDate myDate) {
        return myDate.getDateTime().toString();
    }

    @PostMapping("/validation/user")
    public ValidationUser getValidationUser(@RequestBody @Valid ValidationUser validationUser) {
        return validationUser;
    }
}
