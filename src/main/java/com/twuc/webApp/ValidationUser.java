package com.twuc.webApp;

import sun.rmi.server.InactiveGroupException;

import javax.validation.constraints.*;

public class ValidationUser {
    @NotNull
    private  Integer userId;
    @Size(max = 3, min = 0)
    private String description;
    @Min(0)
    private int min;
    @Max(10)
    private int max;
    @Email
    private String email;

    public ValidationUser() {
    }

    public ValidationUser(@NotNull Integer userId, @Size(max = 3, min = 0) String description, @Min(0) int min, @Max(10) int max, @Email String email) {
        this.userId = userId;
        this.description = description;
        this.min = min;
        this.max = max;
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public String getEmail() {
        return email;
    }
}
