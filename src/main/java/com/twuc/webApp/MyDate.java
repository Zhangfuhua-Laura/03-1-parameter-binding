package com.twuc.webApp;

import java.time.ZonedDateTime;

public class MyDate {
    private ZonedDateTime dateTime;

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public MyDate(ZonedDateTime zonedDateTime) {
        this.dateTime = zonedDateTime;
    }

    public MyDate() {
    }
}
